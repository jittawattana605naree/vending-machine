import React , { Component } from 'react';
import PostForm from './Views/PostForm' ;
import AllPost from './Controllers/AllPost' ;
import AllProduct from './Views/AllProduct';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar , Container ,Nav,NavDropdown,Form,FormControl,Button} from 'react-bootstrap';

class App extends Component {
  render(){
    return (
      <div className="App">
        <Navbar bg="light" expand="lg">
          <Container fluid>
            <Navbar.Brand href="#">Vending Machine</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: '100px' }}
                navbarScroll
              >
                <Nav.Link href="#action1">Home</Nav.Link>
              </Nav>
              <Form className="d-flex">
                <FormControl
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
                <Button variant="outline-success">Search</Button>
              </Form>
            </Navbar.Collapse>
          </Container>
        </Navbar>
          {/* <PostForm />
          <AllPost /> */}
          <AllProduct/>
      </div>
    );
  }
}

export default App;
