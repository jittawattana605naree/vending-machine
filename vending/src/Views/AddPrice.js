import React, { Component } from 'react'
import { Button ,Row , Col,Modal} from 'react-bootstrap';
import { connect } from 'react-redux'

class AddPrice extends Component {
    constructor(props) {
      super(props);
      this.state = { show:false,setShow:false ,money:0};
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = () =>{
      this.setState({
        show:!this.state.show,
      })
    }

    handleAddmoney = (e) =>{
      e.preventDefault();
      const money = this.getmoney.value;
      this.setState({
        money:money,
      })
      this.props.parentCallback(money);
      this.handleSubmit()
    }
  
    
    render() {
        return (
            <Row>
          <Modal
            show={this.state.show}
            onHide={this.handleSubmit}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>Add money</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form className="form" onSubmit={this.handleAddmoney}>
                <input type="text" ref={(input) => this.getmoney = input} placeholder="Enter money" required />
                <button>Add</button>
            </form>
            </Modal.Body>
          </Modal>
                <Col>
                    <h2>{'Price: ' + this.state.money}</h2>
                </Col>
                <Col md={{ span: 1, offset: 1 }}>
                    <Button onClick={this.handleSubmit} variant="outline-success">Add</Button>
                </Col>
            </Row>
        )
    }
}

export default connect()(AddPrice)
