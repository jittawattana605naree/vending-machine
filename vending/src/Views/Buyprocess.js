import React, { Component } from 'react'
import { Modal } from 'react-bootstrap';
import { connect } from 'react-redux'
import { Alert, Col, Row ,Container,Button} from 'react-bootstrap';

class Buyprocess extends Component {
    constructor(props) {
        super(props);
        this.state = { show:false,setShow:false ,money:this.props};
        this.count = 0;
    }
    render() {
        // set zero 
        this.count = 0;
        return (
            <Modal show={this.props.show} 
            onHide={this.props.onHide} 
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
                <Modal.Header closeButton>
                <Modal.Title>Selected</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container bsPrefix='__container'>
                        <Row>
                            <Col>
                            {this.props.posts.map((post) => {
                                this.count += post.element.price
                                return (<div className='card1'>
                                    <Row>
                                        <Col md={1}>
                                            <h4>{post.quantity}</h4>
                                        </Col>
                                        <Col md={{ span: 4, offset: 1 }}>
                                            <h4>{post.element.nameproduct}</h4>
                                        </Col>
                                        
                                        <Col  md={{ offset: 2 }}>
                                            <img className='__image__' src={'http://localhost:4000/'+post.element.url} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <h6>{'Price: '+post.element.price}</h6>
                                    </Row>
                                </div>
                            )}
                            
                            )}
                            </Col>
                            <Col>
                                <Row>
                                     { "Amount : "+this.count }
                                </Row>
                                <Row>
                                    { "Price : " + this.props.money}
                                </Row>
                                <Row>
                                    <Button>Confirm</Button>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            
                        </Row>
                    </Container>
                    
                </Modal.Body>
                <Modal.Footer>
                
                </Modal.Footer>
            </Modal>
        )
    }
    
}

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}


export default connect(mapStateToProps)(Buyprocess);
