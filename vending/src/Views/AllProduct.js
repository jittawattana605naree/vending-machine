import React, { Component } from 'react'
import { Alert, Col, Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Button , Modal} from 'react-bootstrap';
import { connect } from 'react-redux'
import '../assets/css/style.css'
import AllPost from '../Controllers/AllPost';
import AddPrice from './AddPrice';
import Buyprocess from './Buyprocess';


class AllProduct extends Component {

    constructor(props) {
        super(props);
        this.state = { data:[] ,show:false,money:0};
        this.handleSubmit = this.handleSubmit.bind(this);
        // this.Buyprocess = new Buyprocess()
    }

    getapi_allproduct = async() =>{
        let product = await fetch('http://localhost:4000/api/v1/product').then(data => data).catch(e => console.log(e))
        if(product.status == 200){
            const json_response = await product.json();
            this.setState({
                data:json_response.result,
            })
        }
    }

    async componentDidMount(){
        this.getapi_allproduct();
    }

    handleSubmit = (element) => {
        const data = {
            id: element.id,
            quantity: 1,
            element : {...element}
            // editing: false
        }
        this.props.dispatch({
            type: 'ADD_POST',
            data
        })
    }

    handleModal = () =>{
        this.setState({
          show:!this.state.show,
        })
    }
    handleCallback = (childData) =>{
        this.setState({money: childData})

    }

    render() {
        return (
            
            <Container fluid>
                <AddPrice parentCallback={this.handleCallback}/>
                <Row >
                    <Col md={9}>
                        <h2>All Product</h2>
                        <Row>
                            {this.state.data.map((element) => {
                            return (
                                <Col key={element.id} md={4}>
                                    <div key={element.id} className='card'>
                                        <img className='__image' src={'http://localhost:4000/'+element.url} />
                                        <h1 >{element.nameproduct}</h1>
                                        <h3>{'price: '+element.price}</h3>
                                        <Button value={element} onClick={() => this.handleSubmit(element)} variant="outline-success">Add</Button>
                                    </div>
                                </Col>
                            )
                            })}  
                        </Row>
                    </Col>
                    <Col>
                        <h1>Selected</h1>
                        <Row>
                            <AllPost/>
                        </Row>
                        <Row>
                            <Buyprocess show={this.state.show} onHide={this.handleModal} money={this.state.money}/>
                            <Button onClick={this.handleModal}>buy</Button>
                        </Row>
                    </Col>
                </Row>
               
            </Container>
            
        )
    }
}


export default connect()(AllProduct)