import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Alert, Col, Row } from 'react-bootstrap';


class AllPost extends Component {
    render() {
        return (
            <Col>
                {this.props.posts.map((post) => (
                    <div className='card1'>
                        <Row>
                            <Col md={1}>
                                <h4>{post.quantity}</h4>
                            </Col>
                            <Col md={{ span: 4, offset: 1 }}>
                                <h4>{post.element.nameproduct}</h4>
                            </Col>
                            <Col  md={{ offset: 2 }}>
                                <img className='__image__' src={'http://localhost:4000/'+post.element.url} />
                            </Col>
                        </Row>
                       
                    </div>
                   
                ))}
            </Col>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}

export default connect(mapStateToProps)(AllPost);