const dotenv = require('dotenv');
dotenv.config('../../.env');

module.exports = db_config_vending = {
    host: `${process.env.MYSQL_HOST}`,
    user: `${process.env.MYSQL_USER}`,
    password: `${process.env.MYSQL_PASSWORD}`,
    database: `${process.env.MYSQL_DATABASE}`,
    port:process.env.MYSQL_PORT,
}