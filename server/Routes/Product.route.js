const router = require('express').Router();
const ProductControl = require('../controll/product.control');

router.get('/',ProductControl.allProduct);
router.post('/addproduct',ProductControl.insertProductImages().single('file'), ProductControl.insertProduct);

module.exports = router;