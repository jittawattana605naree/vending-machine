const router = require('express').Router();
const MachineControl = require('../controll/machine.control');

router.get('/',MachineControl.allProduct);
router.post('/addmachine',MachineControl.insertProduct);

module.exports = router;