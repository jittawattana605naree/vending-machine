const app = require('express')();
const express = require("express");
var cors = require('cors');
var morgan = require('morgan');
const bodyParser = require("body-parser");
const RouteProduct = require("./Routes/Product.route");
const RouteMachine = require("./Routes/machine.route");
// const flask = require("flask")
const port = 4000;
const http = require('http').Server(app);
app.use(cors());
app.use(bodyParser.urlencoded({
    extended:true
}));
app.use(bodyParser.json());
app.use("/api/v1/product", RouteProduct);
app.use("/api/v1/machine", RouteMachine);
app.use("/uploads", express.static("uploads"));

app.use(morgan('dev'));
app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
});
http.listen(port, () => {
	console.log('Server start in port :' + port);
});

module.exports = app;