const query = require('../config/db-connect');

class ProductModel{
    tablename = 'product';
    
    insertdata = async ({nameproduct="-",price=0,date_of_shipment='-',quantity=0,vending_machine_id=1,url=''}) =>{
        const sql = `INSERT INTO ${this.tablename} (nameproduct, price, date_of_shipment, quantity, vending_machine_id,url)
        VALUES ('${nameproduct}','${price}','${date_of_shipment}','${quantity}','${vending_machine_id}','${url}');`
        const result = await query({sql:sql});
        return await result;
    }

    SelectAlldata = async () =>{
        const sql = `SELECT * FROM ${this.tablename};`
        const result = await query({sql:sql});
        return await result;
    }
}

module.exports = new ProductModel();