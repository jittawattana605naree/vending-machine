const query = require('../config/db-connect');

class MachineModel{
    tablename = 'Machine';
    insertdata = async ({detail="-",address="-"}) =>{
        const sql = `INSERT INTO ${this.tablename} (detail, address)
        VALUES ('${detail}','${address}');`
        const result = await query({sql:sql});
        return await result;
    }

    SelectAlldata = async () =>{
        const sql = `SELECT * FROM ${this.tablename};`
        const result = await query({sql:sql});
        return await result;
    }
}

module.exports = new MachineModel();