const machinemodel =  require('../model/machine.model')

class MachineControl{
    
    allProduct = (req,res,next) =>{
       const result = machinemodel.SelectAlldata()
        .then(response => res.status(200).send({
            status: 200,
            result: response
        }))
        .catch(error => res.status(404).send({
            status: 404,
            message: error
        }))
    }

    insertProduct = (req,res,next) =>{
        const result = machinemodel.insertdata({detail:req.body.detail,address:req.body.address})
        .then( _ => res.status(200).send({
            status: 200,
            message: 'Update success'
        }))
        .catch(error => res.status(404).send({
            status: 404,
            message: 'Update Failed',
            error: error
        }))
    }
}
module.exports = new MachineControl();