const productmodel =  require('../model/product.model')
const multer = require('multer');

class ProductControl{
    
    allProduct = (req,res,next) =>{
       const result = productmodel.SelectAlldata()
        .then(response => res.status(200).send({
            status: 200,
            result: response
        }))
        .catch(error => res.status(404).send({
            status: 404,
            message: error
        }))
    }

    insertProductImages = () => {
        try{
           const storage = multer.diskStorage({
                destination: function (req, file, cb) {
                cb(null, 'uploads/')
                },
                filename: function (req, file, cb) {
                    cb(null, Date.now() + file.originalname )
                }
            })
            const upload = multer({ storage: storage })
            return upload
        }
        catch(error){
            new Error('file Error')
        }
        
    }

    insertProduct = (req, res) =>{
        const result = productmodel.insertdata({
            nameproduct:req.body.nameproduct,
            price:req.body.price,
            date_of_shipment:req.body.date_of_shipment,
            quantity:req.body.quantity,
            vending_machine_id:req.body.vending_machine_id,
            url:req.file.path
        })
        .then( _ => res.status(200).send({
            status: 200,
            message: 'Update success'
        }))
        .catch(error => res.status(404).send({
            status: 404,
            message: 'Update Failed',
            error: error
        }))
    }
}
module.exports = new ProductControl();